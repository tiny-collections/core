plugins {
    `java-library`
    `maven-publish`
}

group = "com.gitlab.tiny-collections"
version = "0.1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

java {
    withJavadocJar()
    withSourcesJar()
}

tasks.compileJava {
    options.release.set(8)
    options.encoding = "UTF-8"
}

tasks.jar {
    manifest {
        attributes(
            "Specification-Title" to "TinyCollections Core API",
            "Specification-Version" to "${project.version}",
            "Automatic-Module-Name" to "TinyCollections.Core"
        )
    }
}

publishing {
    publications {
        create<MavenPublication>("release") {
            groupId = "$group"
            artifactId = "core"
            from(components["java"])
        }
    }
}