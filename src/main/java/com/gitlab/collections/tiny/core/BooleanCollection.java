package com.gitlab.collections.tiny.core;

import com.gitlab.collections.tiny.core.iterator.BooleanIterator;

import java.util.Spliterator;
import java.util.stream.Stream;

/**
 * The root of the ByteCollection hierarchy. Represents a group of int values. Implementations may allow duplicate
 * values, may be ordered or unordered, may allow or deny modifications, and may or may not be thread-safe.
 */
public interface BooleanCollection extends PrimitiveCollection {
    /**
     * Returns true if this collection contains the specified value. Returns true only if at least one element
     * {@code e == i}.
     *
     * @param i The value to check the presence of.
     * @return True if this collection contains the element.
     */
    boolean contains(boolean i);

    /**
     * Returns an iterator over the elements in this collection.
     *
     * @return An iterator over the elements in this collection.
     */
    BooleanIterator iterator();

    /**
     * Returns an array containing all the elements of the collection. No reference to the returned array will be
     * retained by the collection.
     *
     * @return An array containing all the elements of the collection.
     */
    boolean[] toArray();

    /**
     * If the provided array is large enough to contain all the elements of this collection, then the array will be
     * filled from index 0 with the collection's elements. If the provided array is not large enough a new array will
     * be created.
     *
     * @param a The array to (possibly) fill
     * @return The provided array, containing the collection's elements. Or a new array containing the collection's
     * elements
     */
    boolean[] toArray(boolean[] a);

    // Bulk Operations

    /**
     * Returns true if this collection contains all the elements in the other collection.
     *
     * @param c The collection with elements to check this collection for.
     * @return True if this collection contains all the elements in the provided collection.
     * @see #containsAll(boolean[])
     */
    default boolean containsAll(BooleanCollection c) {
        final BooleanIterator itr = c.iterator();
        while (itr.hasNext()) {
            if (!contains(itr.nextBoolean())) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns true if this collection contains all the elements in the provided array.
     *
     * @param a The array with elements to check this collection for.
     * @return True if this collection contains all the elements in the provided array.
     * @see #containsAll(BooleanCollection)
     */
    default boolean containsAll(boolean[] a) {
        for (boolean b : a) {
            if (!contains(b)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Creates a specialized spliterator over the elements in this collection.
     *
     * @return A spliterator over the elements in this collection.
     */
    Spliterator<Boolean> spliterator();

    /**
     * A sequential stream with the collection elements as its source.
     *
     * @return A sequential stream of the collection's elements.
     */
    Stream<Boolean> stream();

    /**
     * A possibly parallel stream with the collection elements as its source.
     *
     * @return A possibly parallel stream of the collection's elements.
     */
    Stream<Boolean> parallelStream();
}
