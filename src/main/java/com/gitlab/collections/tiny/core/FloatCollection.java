package com.gitlab.collections.tiny.core;

import com.gitlab.collections.tiny.core.iterator.FloatIterator;

import java.util.Spliterator;
import java.util.stream.Stream;

/**
 * The root of the FloatCollection hierarchy. Represents a group of double values. Implementations may allow duplicate
 * values, may be ordered or unordered, may allow or deny modifications, and may or may not be thread-safe.
 */
public interface FloatCollection extends PrimitiveCollection {
    /**
     * Returns true if this collection contains the specified value. Returns true only if at least one element
     * {@code e == f}.
     *
     * @param f The value to check the presence of.
     * @return True if this collection contains the element.
     */
    boolean contains(float f);

    /**
     * Returns an iterator over the elements in this collection.
     *
     * @return An iterator over the elements in this collection.
     */
    FloatIterator iterator();

    /**
     * Returns an array containing all the elements of the collection. No reference to the returned array will be
     * retained by the collection.
     *
     * @return An array containing all the elements of the collection.
     */
    float[] toArray();

    /**
     * If the provided array is large enough to contain all the elements of this collection, then the array will be
     * filled from index 0 with the collection's elements. If the provided array is not large enough a new array will
     * be created.
     *
     * @param a The array to (possibly) fill
     * @return The provided array, containing the collection's elements. Or a new array containing the collection's
     * elements
     */
    float[] toArray(float[] a);

    // Bulk Operations

    /**
     * Returns true if this collection contains all the elements in the other collection.
     *
     * @param c The collection with elements to check this collection for.
     * @return True if this collection contains all the elements in the provided collection.
     * @see #containsAll(float[])
     */
    default boolean containsAll(FloatCollection c) {
        final FloatIterator itr = c.iterator();
        while (itr.hasNext()) {
            if (!contains(itr.nextFloat())) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns true if this collection contains all the elements in the provided array.
     *
     * @param a The array with elements to check this collection for.
     * @return True if this collection contains all the elements in the provided array.
     * @see #containsAll(FloatCollection)
     */
    default boolean containsAll(float[] a) {
        for (float i : a) {
            if (!contains(i)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Creates a specialized spliterator over the elements in this collection.
     *
     * @return A spliterator over the elements in this collection.
     */
    Spliterator<Float> spliterator();

    /**
     * A sequential stream with the collection elements as its source.
     *
     * @return A sequential stream of the collection's elements.
     */
    Stream<Float> stream();

    /**
     * A possibly parallel stream with the collection elements as its source.
     *
     * @return A possibly parallel stream of the collection's elements.
     */
    Stream<Float> parallelStream();
}
