package com.gitlab.collections.tiny.core;

import com.gitlab.collections.tiny.core.function.BooleanPredicate;
import com.gitlab.collections.tiny.core.iterator.BooleanIterator;

import java.util.Objects;

/**
 * Extension of the {@link BooleanCollection} interface to indicate that this collection can be modified after creation.
 */
public interface ModifiableBooleanCollection extends BooleanCollection {
    /**
     * Include a value in this collection. Implementations may place restrictions on where the value will be inserted.
     *
     * @param i The value to include.
     * @return True if this collection changed.
     */
    boolean add(boolean i);

    /**
     * Removes a single value in this collection. Implementations which allow duplicate values must only ever remove one
     * instance per call.
     *
     * @param i The value to remove.
     * @return True if this collection changed.
     */
    boolean remove(boolean i);

    // Bulk methods

    /**
     * Adds all the elements of the provided collection to this collection.
     *
     * @param c The collection of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(BooleanCollection c) {
        final BooleanIterator itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (add(itr.nextBoolean())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Adds all the elements of the provided array to this collection.
     *
     * @param a The array of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(boolean[] a) {
        boolean changed = false;
        for (boolean b : a) {
            if (add(b)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided collection from this collection.
     *
     * @param c The collection of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(BooleanCollection c) {
        final BooleanIterator itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (remove(itr.nextBoolean())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided array from this collection.
     *
     * @param a The array of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(boolean[] a) {
        boolean changed = false;
        for (boolean b : a) {
            if (remove(b)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each value from this collection that satisfies the provided predicate. Exceptions in the predicate are
     * relayed to the caller.
     *
     * @param filter A predicate that returns true for element to remove.
     * @return True if this collection changed.
     */
    default boolean removeIf(BooleanPredicate filter) {
        Objects.requireNonNull(filter);

        boolean changed = false;
        BooleanIterator itr = iterator();
        while (itr.hasNext()) {
            if (filter.test(itr.nextBoolean())) {
                itr.remove();
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided collection.
     *
     * @param c The collection of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(BooleanCollection c) {
        final BooleanIterator thisItr = iterator();
        boolean changed = false;

        while (thisItr.hasNext()) {
            boolean value = thisItr.nextBoolean();
            if (!c.contains(value)) {
                if (remove(value)) {
                    changed = true;
                }
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided array.
     *
     * @param a The array of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(boolean[] a) {
        final BooleanIterator thisItr = iterator();
        boolean changed = false;

        OUTER:
        while (thisItr.hasNext()) {
            boolean value = thisItr.nextBoolean();

            for (boolean b : a) {
                if (b == value) {
                    continue OUTER;
                }
            }

            if (remove(value)) {
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes all elements from this collection.
     */
    void clear();
}
