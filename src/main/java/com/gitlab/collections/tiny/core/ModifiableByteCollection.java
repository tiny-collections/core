package com.gitlab.collections.tiny.core;

import com.gitlab.collections.tiny.core.function.BytePredicate;
import com.gitlab.collections.tiny.core.iterator.ByteIterator;

import java.util.Objects;

/**
 * Extension of the {@link ByteCollection} interface to indicate that this collection can be modified after creation.
 */
public interface ModifiableByteCollection extends ByteCollection {
    /**
     * Include a value in this collection. Implementations may place restrictions on where the value will be inserted.
     *
     * @param i The value to include.
     * @return True if this collection changed.
     */
    boolean add(byte i);

    /**
     * Removes a single value in this collection. Implementations which allow duplicate values must only ever remove one
     * instance per call.
     *
     * @param i The value to remove.
     * @return True if this collection changed.
     */
    boolean remove(byte i);

    // Bulk methods

    /**
     * Adds all the elements of the provided collection to this collection.
     *
     * @param c The collection of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(ByteCollection c) {
        final ByteIterator itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (add(itr.nextByte())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Adds all the elements of the provided array to this collection.
     *
     * @param a The array of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(byte[] a) {
        boolean changed = false;
        for (byte b : a) {
            if (add(b)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided collection from this collection.
     *
     * @param c The collection of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(ByteCollection c) {
        final ByteIterator itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (remove(itr.nextByte())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided array from this collection.
     *
     * @param a The array of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(byte[] a) {
        boolean changed = false;
        for (byte b : a) {
            if (remove(b)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each value from this collection that satisfies the provided predicate. Exceptions in the predicate are
     * relayed to the caller.
     *
     * @param filter A predicate that returns true for element to remove.
     * @return True if this collection changed.
     */
    default boolean removeIf(BytePredicate filter) {
        Objects.requireNonNull(filter);

        boolean changed = false;
        ByteIterator itr = iterator();
        while (itr.hasNext()) {
            if (filter.test(itr.nextByte())) {
                itr.remove();
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided collection.
     *
     * @param c The collection of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(ByteCollection c) {
        final ByteIterator thisItr = iterator();
        boolean changed = false;

        while (thisItr.hasNext()) {
            byte value = thisItr.nextByte();
            if (!c.contains(value)) {
                if (remove(value)) {
                    changed = true;
                }
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided array.
     *
     * @param a The array of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(byte[] a) {
        final ByteIterator thisItr = iterator();
        boolean changed = false;

        OUTER:
        while (thisItr.hasNext()) {
            byte value = thisItr.nextByte();

            for (byte b : a) {
                if (b == value) {
                    continue OUTER;
                }
            }

            if (remove(value)) {
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes all elements from this collection.
     */
    void clear();
}
