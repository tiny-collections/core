package com.gitlab.collections.tiny.core;

import com.gitlab.collections.tiny.core.function.CharPredicate;
import com.gitlab.collections.tiny.core.iterator.CharIterator;

import java.util.Objects;

/**
 * Extension of the {@link CharCollection} interface to indicate that this collection can be modified after creation.
 */
public interface ModifiableCharCollection extends CharCollection {
    /**
     * Include a value in this collection. Implementations may place restrictions on where the value will be inserted.
     *
     * @param c The value to include.
     * @return True if this collection changed.
     */
    boolean add(char c);

    /**
     * Removes a single value in this collection. Implementations which allow duplicate values must only ever remove one
     * instance per call.
     *
     * @param c The value to remove.
     * @return True if this collection changed.
     */
    boolean remove(char c);

    // Bulk methods

    /**
     * Adds all the elements of the provided collection to this collection.
     *
     * @param c The collection of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(CharCollection c) {
        final CharIterator itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (add(itr.nextChar())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Adds all the elements of the provided array to this collection.
     *
     * @param a The array of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(char[] a) {
        boolean changed = false;
        for (char c : a) {
            if (add(c)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided collection from this collection.
     *
     * @param c The collection of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(CharCollection c) {
        final CharIterator itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (remove(itr.nextChar())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided array from this collection.
     *
     * @param a The array of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(char[] a) {
        boolean changed = false;
        for (char c : a) {
            if (remove(c)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each value from this collection that satisfies the provided predicate. Exceptions in the predicate are
     * relayed to the caller.
     *
     * @param filter A predicate that returns true for element to remove.
     * @return True if this collection changed.
     */
    default boolean removeIf(CharPredicate filter) {
        Objects.requireNonNull(filter);

        boolean changed = false;
        CharIterator itr = iterator();
        while (itr.hasNext()) {
            if (filter.test(itr.nextChar())) {
                itr.remove();
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided collection.
     *
     * @param c The collection of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(CharCollection c) {
        final CharIterator thisItr = iterator();
        boolean changed = false;

        while (thisItr.hasNext()) {
            char value = thisItr.nextChar();
            if (!c.contains(value)) {
                if (remove(value)) {
                    changed = true;
                }
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided array.
     *
     * @param a The array of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(char[] a) {
        final CharIterator thisItr = iterator();
        boolean changed = false;

        OUTER:
        while (thisItr.hasNext()) {
            char value = thisItr.nextChar();

            for (char b : a) {
                if (b == value) {
                    continue OUTER;
                }
            }

            if (remove(value)) {
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes all elements from this collection.
     */
    void clear();
}
