package com.gitlab.collections.tiny.core;

import java.util.Objects;
import java.util.PrimitiveIterator;
import java.util.function.DoublePredicate;

/**
 * Extension of the {@link DoubleCollection} interface to indicate that this collection can be modified after creation.
 */
public interface ModifiableDoubleCollection extends DoubleCollection {
    /**
     * Include a value in this collection. Implementations may place restrictions on where the value will be inserted.
     *
     * @param d The value to include.
     * @return True if this collection changed.
     */
    boolean add(double d);

    /**
     * Removes a single value in this collection. Implementations which allow duplicate values must only ever remove one
     * instance per call.
     *
     * @param d The value to remove.
     * @return True if this collection changed.
     */
    boolean remove(double d);

    // Bulk methods

    /**
     * Adds all the elements of the provided collection to this collection.
     *
     * @param c The collection of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(DoubleCollection c) {
        final PrimitiveIterator.OfDouble itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (add(itr.nextDouble())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Adds all the elements of the provided array to this collection.
     *
     * @param a The array of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(double[] a) {
        boolean changed = false;
        for (double b : a) {
            if (add(b)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided collection from this collection.
     *
     * @param c The collection of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(DoubleCollection c) {
        final PrimitiveIterator.OfDouble itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (remove(itr.nextDouble())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided array from this collection.
     *
     * @param a The array of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(double[] a) {
        boolean changed = false;
        for (double d : a) {
            if (remove(d)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each value from this collection that satisfies the provided predicate. Exceptions in the predicate are
     * relayed to the caller.
     *
     * @param filter A predicate that returns true for element to remove.
     * @return True if this collection changed.
     */
    default boolean removeIf(DoublePredicate filter) {
        Objects.requireNonNull(filter);

        boolean changed = false;
        PrimitiveIterator.OfDouble itr = iterator();
        while (itr.hasNext()) {
            if (filter.test(itr.nextDouble())) {
                itr.remove();
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided collection.
     *
     * @param c The collection of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(DoubleCollection c) {
        final PrimitiveIterator.OfDouble thisItr = iterator();
        boolean changed = false;

        while (thisItr.hasNext()) {
            double value = thisItr.nextDouble();
            if (!c.contains(value)) {
                if (remove(value)) {
                    changed = true;
                }
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided array.
     *
     * @param a The array of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(double[] a) {
        final PrimitiveIterator.OfDouble thisItr = iterator();
        boolean changed = false;

        OUTER:
        while (thisItr.hasNext()) {
            double value = thisItr.nextDouble();

            for (double b : a) {
                if (b == value) {
                    continue OUTER;
                }
            }

            if (remove(value)) {
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes all elements from this collection.
     */
    void clear();
}
