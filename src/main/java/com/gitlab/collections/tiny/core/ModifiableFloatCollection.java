package com.gitlab.collections.tiny.core;

import com.gitlab.collections.tiny.core.function.FloatPredicate;
import com.gitlab.collections.tiny.core.iterator.FloatIterator;

import java.util.Objects;

/**
 * Extension of the {@link FloatCollection} interface to indicate that this collection can be modified after creation.
 */
public interface ModifiableFloatCollection extends FloatCollection {
    /**
     * Include a value in this collection. Implementations may place restrictions on where the value will be inserted.
     *
     * @param f The value to include.
     * @return True if this collection changed.
     */
    boolean add(float f);

    /**
     * Removes a single value in this collection. Implementations which allow duplicate values must only ever remove one
     * instance per call.
     *
     * @param f The value to remove.
     * @return True if this collection changed.
     */
    boolean remove(float f);

    // Bulk methods

    /**
     * Adds all the elements of the provided collection to this collection.
     *
     * @param c The collection of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(FloatCollection c) {
        final FloatIterator itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (add(itr.nextFloat())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Adds all the elements of the provided array to this collection.
     *
     * @param a The array of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(float[] a) {
        boolean changed = false;
        for (float f : a) {
            if (add(f)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided collection from this collection.
     *
     * @param c The collection of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(FloatCollection c) {
        final FloatIterator itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (remove(itr.nextFloat())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided array from this collection.
     *
     * @param a The array of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(float[] a) {
        boolean changed = false;
        for (float b : a) {
            if (remove(b)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each value from this collection that satisfies the provided predicate. Exceptions in the predicate are
     * relayed to the caller.
     *
     * @param filter A predicate that returns true for element to remove.
     * @return True if this collection changed.
     */
    default boolean removeIf(FloatPredicate filter) {
        Objects.requireNonNull(filter);

        boolean changed = false;
        FloatIterator itr = iterator();
        while (itr.hasNext()) {
            if (filter.test(itr.nextFloat())) {
                itr.remove();
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided collection.
     *
     * @param c The collection of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(FloatCollection c) {
        final FloatIterator thisItr = iterator();
        boolean changed = false;

        while (thisItr.hasNext()) {
            float value = thisItr.nextFloat();
            if (!c.contains(value)) {
                if (remove(value)) {
                    changed = true;
                }
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided array.
     *
     * @param a The array of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(float[] a) {
        final FloatIterator thisItr = iterator();
        boolean changed = false;

        OUTER:
        while (thisItr.hasNext()) {
            float value = thisItr.nextFloat();

            for (float b : a) {
                if (b == value) {
                    continue OUTER;
                }
            }

            if (remove(value)) {
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes all elements from this collection.
     */
    void clear();
}
