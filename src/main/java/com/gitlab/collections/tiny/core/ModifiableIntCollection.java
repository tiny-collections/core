package com.gitlab.collections.tiny.core;

import java.util.Objects;
import java.util.PrimitiveIterator;
import java.util.function.IntPredicate;

/**
 * Extension of the {@link IntCollection} interface to indicate that this collection can be modified after creation.
 */
public interface ModifiableIntCollection extends IntCollection {
    /**
     * Include a value in this collection. Implementations may place restrictions on where the value will be inserted.
     *
     * @param i The value to include.
     * @return True if this collection changed.
     */
    boolean add(int i);

    /**
     * Removes a single value in this collection. Implementations which allow duplicate values must only ever remove one
     * instance per call.
     *
     * @param i The value to remove.
     * @return True if this collection changed.
     */
    boolean remove(int i);

    // Bulk methods

    /**
     * Adds all the elements of the provided collection to this collection.
     *
     * @param c The collection of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(IntCollection c) {
        final PrimitiveIterator.OfInt itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (add(itr.nextInt())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Adds all the elements of the provided array to this collection.
     *
     * @param a The array of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(int[] a) {
        boolean changed = false;
        for (int i : a) {
            if (add(i)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided collection from this collection.
     *
     * @param c The collection of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(IntCollection c) {
        final PrimitiveIterator.OfInt itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (remove(itr.nextInt())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided array from this collection.
     *
     * @param a The array of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(int[] a) {
        boolean changed = false;
        for (int i : a) {
            if (remove(i)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each value from this collection that satisfies the provided predicate. Exceptions in the predicate are
     * relayed to the caller.
     *
     * @param filter A predicate that returns true for element to remove.
     * @return True if this collection changed.
     */
    default boolean removeIf(IntPredicate filter) {
        Objects.requireNonNull(filter);

        boolean changed = false;
        PrimitiveIterator.OfInt itr = iterator();
        while (itr.hasNext()) {
            if (filter.test(itr.nextInt())) {
                itr.remove();
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided collection.
     *
     * @param c The collection of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(IntCollection c) {
        final PrimitiveIterator.OfInt thisItr = iterator();
        boolean changed = false;

        while (thisItr.hasNext()) {
            int value = thisItr.nextInt();
            if (!c.contains(value)) {
                if (remove(value)) {
                    changed = true;
                }
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided array.
     *
     * @param a The array of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(int[] a) {
        final PrimitiveIterator.OfInt thisItr = iterator();
        boolean changed = false;

        OUTER:
        while (thisItr.hasNext()) {
            int value = thisItr.nextInt();

            for (int i : a) {
                if (i == value) {
                    continue OUTER;
                }
            }

            if (remove(value)) {
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes all elements from this collection.
     */
    void clear();
}
