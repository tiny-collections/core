package com.gitlab.collections.tiny.core;

import java.util.Objects;
import java.util.PrimitiveIterator;
import java.util.function.LongPredicate;

/**
 * Extension of the {@link LongCollection} interface to indicate that this collection can be modified after creation.
 */
public interface ModifiableLongCollection extends LongCollection {
    /**
     * Include a value in this collection. Implementations may place restrictions on where the value will be inserted.
     *
     * @param i The value to include.
     * @return True if this collection changed.
     */
    boolean add(long i);

    /**
     * Removes a single value in this collection. Implementations which allow duplicate values must only ever remove one
     * instance per call.
     *
     * @param i The value to remove.
     * @return True if this collection changed.
     */
    boolean remove(long i);

    // Bulk Operations

    /**
     * Adds all the elements of the provided collection to this collection.
     *
     * @param c The collection of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(LongCollection c) {
        final PrimitiveIterator.OfLong itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (add(itr.nextLong())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Adds all the elements of the provided array to this collection.
     *
     * @param a The array of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(long[] a) {
        boolean changed = false;
        for (long l : a) {
            if (add(l)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided collection from this collection.
     *
     * @param c The collection of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(LongCollection c) {
        final PrimitiveIterator.OfLong itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (remove(itr.nextLong())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided array from this collection.
     *
     * @param a The array of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(long[] a) {
        boolean changed = false;
        for (long l : a) {
            if (remove(l)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each value from this collection that satisfies the provided predicate. Exceptions in the predicate are
     * relayed to the caller.
     *
     * @param filter A predicate that returns true for element to remove.
     * @return True if this collection changed.
     */
    default boolean removeIf(LongPredicate filter) {
        Objects.requireNonNull(filter);

        boolean changed = false;
        PrimitiveIterator.OfLong itr = iterator();
        while (itr.hasNext()) {
            if (filter.test(itr.nextLong())) {
                itr.remove();
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided collection.
     *
     * @param c The collection of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(LongCollection c) {
        final PrimitiveIterator.OfLong thisItr = iterator();
        boolean changed = false;

        while (thisItr.hasNext()) {
            long value = thisItr.nextLong();
            if (!c.contains(value)) {
                if (remove(value)) {
                    changed = true;
                }
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided array.
     *
     * @param a The array of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(long[] a) {
        final PrimitiveIterator.OfLong thisItr = iterator();
        boolean changed = false;

        OUTER:
        while (thisItr.hasNext()) {
            long value = thisItr.nextLong();

            for (long l : a) {
                if (l == value) {
                    continue OUTER;
                }
            }

            if (remove(value)) {
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes all elements from this collection.
     */
    void clear();
}
