package com.gitlab.collections.tiny.core;

import com.gitlab.collections.tiny.core.function.ShortPredicate;
import com.gitlab.collections.tiny.core.iterator.ShortIterator;

import java.util.Objects;

/**
 * Extension of the {@link ShortCollection} interface to indicate that this collection can be modified after creation.
 */
public interface ModifiableShortCollection extends ShortCollection {
    /**
     * Include a value in this collection. Implementations may place restrictions on where the value will be inserted.
     *
     * @param i The value to include.
     * @return True if this collection changed.
     */
    boolean add(short i);

    /**
     * Removes a single value in this collection. Implementations which allow duplicate values must only ever remove one
     * instance per call.
     *
     * @param i The value to remove.
     * @return True if this collection changed.
     */
    boolean remove(short i);

    // Bulk methods

    /**
     * Adds all the elements of the provided collection to this collection.
     *
     * @param c The collection of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(ShortCollection c) {
        final ShortIterator itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (add(itr.nextShort())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Adds all the elements of the provided array to this collection.
     *
     * @param a The array of values to include.
     * @return True if this collection changed.
     */
    default boolean addAll(short[] a) {
        boolean changed = false;
        for (short s : a) {
            if (add(s)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided collection from this collection.
     *
     * @param c The collection of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(ShortCollection c) {
        final ShortIterator itr = c.iterator();
        boolean changed = false;
        while (itr.hasNext()) {
            if (remove(itr.nextShort())) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each element of the provided array from this collection.
     *
     * @param a The array of values to remove.
     * @return True if this collection changed.
     */
    default boolean removeAll(short[] a) {
        boolean changed = false;
        for (short s : a) {
            if (remove(s)) {
                changed = true;
            }
        }
        return changed;
    }

    /**
     * Removes each value from this collection that satisfies the provided predicate. Exceptions in the predicate are
     * relayed to the caller.
     *
     * @param filter A predicate that returns true for element to remove.
     * @return True if this collection changed.
     */
    default boolean removeIf(ShortPredicate filter) {
        Objects.requireNonNull(filter);

        boolean changed = false;
        ShortIterator itr = iterator();
        while (itr.hasNext()) {
            if (filter.test(itr.nextShort())) {
                itr.remove();
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided collection.
     *
     * @param c The collection of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(ShortCollection c) {
        final ShortIterator thisItr = iterator();
        boolean changed = false;

        while (thisItr.hasNext()) {
            short value = thisItr.nextShort();
            if (!c.contains(value)) {
                if (remove(value)) {
                    changed = true;
                }
            }
        }

        return changed;
    }

    /**
     * Removes any element from this collection that does not also appear in the provided array.
     *
     * @param a The array of values to keep.
     * @return True if this collection changed.
     */
    default boolean retainAll(short[] a) {
        final ShortIterator thisItr = iterator();
        boolean changed = false;

        OUTER:
        while (thisItr.hasNext()) {
            short value = thisItr.nextShort();

            for (short s : a) {
                if (s == value) {
                    continue OUTER;
                }
            }

            if (remove(value)) {
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Removes all elements from this collection.
     */
    void clear();
}
