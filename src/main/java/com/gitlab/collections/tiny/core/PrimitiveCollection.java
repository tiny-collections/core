package com.gitlab.collections.tiny.core;

/**
 * Represents an object which contains multiple primitive values.
 */
public interface PrimitiveCollection {
    /**
     * Returns the number of elements in this collection.
     *
     * @return The number of elements in this collection.
     */
    int size();

    /**
     * Returns true if this collection contains no elements.
     *
     * @return True if this collection contains no elements.
     */
    default boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Compares the equality of an object to this collection.
     * <p>
     * The exact specification depends on the collection subtype, but generally two collections are equal if they
     * both contain the same elements. Some subtypes may view the order of the elements to form part of their equals
     * specification.
     *
     * @param o The object to compare against this collection.
     * @return True if the collections share equality with each other.
     */
    boolean equals(Object o);

    /**
     * The collection's hash code depends on its contents so that when {@code o1.equals(o2)},
     * {@code o1.hashCode() == o2.hashCode()}
     *
     * @return The object's hash code
     */
    int hashCode();
}
