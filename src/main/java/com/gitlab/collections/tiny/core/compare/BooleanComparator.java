package com.gitlab.collections.tiny.core.compare;

/**
 * Compares its two arguments for order.
 */
@FunctionalInterface
public interface BooleanComparator {
    /**
     * Compares its two arguments for order. Returns a negative integer, zero, or a positive integer as the first
     * argument is less than, equal to, or greater than the second.
     *
     * @param i1 The first value to compare
     * @param i2 The second value to compare
     * @return A negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the second.
     */
    int compare(boolean i1, boolean i2);

    /**
     * @return A comparator with the reverse order of this comparator.
     */
    default BooleanComparator reversed() {
        return (i1, i2) -> compare(i2, i1);
    }
}
