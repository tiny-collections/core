package com.gitlab.collections.tiny.core.compare;

public class PrimitiveComparators {
    private static final ByteComparator BYTE_NATURAL = Byte::compare;
    private static final ByteComparator BYTE_NATURAL_REVERSE = BYTE_NATURAL.reversed();

    private static final ShortComparator SHORT_NATURAL = Short::compare;
    private static final ShortComparator SHORT_NATURAL_REVERSE = SHORT_NATURAL.reversed();

    private static final IntComparator INT_NATURAL = Integer::compare;
    private static final IntComparator INT_NATURAL_REVERSE = INT_NATURAL.reversed();

    private static final LongComparator LONG_NATURAL = Long::compare;
    private static final LongComparator LONG_NATURAL_REVERSE = LONG_NATURAL.reversed();

    private static final FloatComparator FLOAT_NATURAL = Float::compare;
    private static final FloatComparator FLOAT_NATURAL_REVERSE = FLOAT_NATURAL.reversed();

    private static final DoubleComparator DOUBLE_NATURAL = Double::compare;
    private static final DoubleComparator DOUBLE_NATURAL_REVERSE = DOUBLE_NATURAL.reversed();

    private static final BooleanComparator BOOLEAN_NATURAL = Boolean::compare;
    private static final BooleanComparator BOOLEAN_NATURAL_REVERSE = BOOLEAN_NATURAL.reversed();

    private static final CharComparator CHAR_NATURAL = Character::compare;
    private static final CharComparator CHAR_NATURAL_REVERSE = CHAR_NATURAL.reversed();

    public static ByteComparator naturalOrderByte() {
        return BYTE_NATURAL;
    }

    public static ByteComparator reverseNaturalOrderByte() {
        return BYTE_NATURAL_REVERSE;
    }

    public static ShortComparator naturalOrderShort() {
        return SHORT_NATURAL;
    }

    public static ShortComparator reverseNaturalOrderShort() {
        return SHORT_NATURAL_REVERSE;
    }

    public static IntComparator naturalOrderInt() {
        return INT_NATURAL;
    }

    public static IntComparator reverseNaturalOrderInt() {
        return INT_NATURAL_REVERSE;
    }

    public static LongComparator naturalOrderLong() {
        return LONG_NATURAL;
    }

    public static LongComparator reverseNaturalOrderLong() {
        return LONG_NATURAL_REVERSE;
    }

    public static FloatComparator naturalOrderFloat() {
        return FLOAT_NATURAL;
    }

    public static FloatComparator reverseNaturalOrderFloat() {
        return FLOAT_NATURAL_REVERSE;
    }

    public static DoubleComparator naturalOrderDouble() {
        return DOUBLE_NATURAL;
    }

    public static DoubleComparator reverseNaturalOrderDouble() {
        return DOUBLE_NATURAL_REVERSE;
    }

    public static BooleanComparator naturalOrderBoolean() {
        return BOOLEAN_NATURAL;
    }

    public static BooleanComparator reverseNaturalOrderBoolean() {
        return BOOLEAN_NATURAL_REVERSE;
    }

    public static CharComparator naturalOrderChar() {
        return CHAR_NATURAL;
    }

    public static CharComparator reverseNaturalOrderChar() {
        return CHAR_NATURAL_REVERSE;
    }

    private PrimitiveComparators() {}
}
