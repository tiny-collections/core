package com.gitlab.collections.tiny.core.function;

import java.util.Objects;

/**
 * A boolean-valued function of one boolean-valued argument.
 *
 * @see java.util.function.Predicate
 */
public interface BooleanPredicate {
    /**
     * Evaluates this predicate against an input.
     *
     * @param value The input argument.
     * @return True if the argument matches the predicate.
     */
    boolean test(boolean value);

    /**
     * Composes a function that represents a short-circuiting logical AND of this predicate and another.
     *
     * @param other A predicate that will be ANDed with this predicate.
     * @return A composed predicate of this short-circuit ANDed with the other predicate.
     */
    default BooleanPredicate and(BooleanPredicate other) {
        Objects.requireNonNull(other);
        return v -> test(v) && other.test(v);
    }

    /**
     * Returns a predicate that is the logical negation of this predicate
     *
     * @return A predicate that is the logical negation of this predicate
     */
    default BooleanPredicate negate() {
        return v -> !test(v);
    }

    /**
     * Composes a function that represents a short-circuiting logical OR of this predicate and another.
     *
     * @param other A predicate that will be ORed with this predicate.
     * @return A composed predicate of this short-circuit ORed with the other predicate.
     */
    default BooleanPredicate or(BooleanPredicate other) {
        Objects.requireNonNull(other);
        return v -> test(v) || other.test(v);
    }
}
