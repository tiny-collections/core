package com.gitlab.collections.tiny.core.function;

import java.util.Objects;

/**
 * A specialized form of the {@link java.util.function.Consumer} interface for chars.
 */
@FunctionalInterface
public interface CharConsumer {
    /**
     * Performs an action of the argument.
     *
     * @param value The input argument
     */
    void accept(char value);

    /**
     * Returns a composed CharConsumer that performs this action followed by the after function.
     *
     * @param after The operation to perform after this.
     * @return A composed consumer that performs this then after.
     * @throws NullPointerException If after is null.
     */
    default CharConsumer andThen(CharConsumer after) {
        Objects.requireNonNull(after);
        return f -> {
            accept(f);
            after.accept(f);
        };
    }
}
