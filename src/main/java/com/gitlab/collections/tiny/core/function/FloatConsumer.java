package com.gitlab.collections.tiny.core.function;

import java.util.Objects;

/**
 * A specialized form of the {@link java.util.function.Consumer} interface for floats.
 */
@FunctionalInterface
public interface FloatConsumer {
    /**
     * Performs an action of the argument.
     *
     * @param value The input argument
     */
    void accept(float value);

    /**
     * Returns a composed FloatConsumer that performs this action followed by the after function.
     *
     * @param after The operation to perform after this.
     * @return A composed consumer that performs this then after.
     * @throws NullPointerException If after is null.
     */
    default FloatConsumer andThen(FloatConsumer after) {
        Objects.requireNonNull(after);
        return f -> {
            accept(f);
            after.accept(f);
        };
    }
}
