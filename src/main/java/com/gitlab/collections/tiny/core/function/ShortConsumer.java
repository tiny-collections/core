package com.gitlab.collections.tiny.core.function;

import java.util.Objects;

/**
 * A specialized form of the {@link java.util.function.Consumer} interface for shorts.
 */
@FunctionalInterface
public interface ShortConsumer {
    /**
     * Performs an action of the argument.
     *
     * @param value The input argument
     */
    void accept(short value);

    /**
     * Returns a composed ShortConsumer that performs this action followed by the after function.
     *
     * @param after The operation to perform after this.
     * @return A composed consumer that performs this then after.
     * @throws NullPointerException If after is null.
     */
    default ShortConsumer andThen(ShortConsumer after) {
        Objects.requireNonNull(after);
        return f -> {
            accept(f);
            after.accept(f);
        };
    }
}
