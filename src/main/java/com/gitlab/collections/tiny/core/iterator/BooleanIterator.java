package com.gitlab.collections.tiny.core.iterator;

import com.gitlab.collections.tiny.core.function.BooleanConsumer;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.PrimitiveIterator;

/**
 * A specialized iterator for booleans
 */
public interface BooleanIterator extends PrimitiveIterator<Boolean, BooleanConsumer> {
    /**
     * Returns the next boolean and advances the iterator.
     *
     * @return The next boolean.
     * @throws NoSuchElementException If there are no more elements.
     */
    boolean nextBoolean();

    default void forEachRemaining(BooleanConsumer action) {
        Objects.requireNonNull(action);
        while (hasNext()) {
            action.accept(nextBoolean());
        }
    }
}
