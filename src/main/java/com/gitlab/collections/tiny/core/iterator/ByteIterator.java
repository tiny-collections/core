package com.gitlab.collections.tiny.core.iterator;

import com.gitlab.collections.tiny.core.function.ByteConsumer;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.PrimitiveIterator;

/**
 * A specialized iterator for bytes
 */
public interface ByteIterator extends PrimitiveIterator<Byte, ByteConsumer> {
    /**
     * Returns the next byte and advances the iterator.
     *
     * @return The next byte.
     * @throws NoSuchElementException If there are no more elements.
     */
    byte nextByte();

    default void forEachRemaining(ByteConsumer action) {
        Objects.requireNonNull(action);
        while (hasNext()) {
            action.accept(nextByte());
        }
    }
}
