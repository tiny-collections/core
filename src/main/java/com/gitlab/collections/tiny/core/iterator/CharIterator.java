package com.gitlab.collections.tiny.core.iterator;

import com.gitlab.collections.tiny.core.function.CharConsumer;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.PrimitiveIterator;

/**
 * A specialized iterator for chars
 */
public interface CharIterator extends PrimitiveIterator<Character, CharConsumer> {
    /**
     * Returns the next char and advances the iterator.
     *
     * @return The next char.
     * @throws NoSuchElementException If there are no more elements.
     */
    char nextChar();

    default void forEachRemaining(CharConsumer action) {
        Objects.requireNonNull(action);
        while (hasNext()) {
            action.accept(nextChar());
        }
    }
}
