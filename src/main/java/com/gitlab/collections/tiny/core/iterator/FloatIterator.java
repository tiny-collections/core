package com.gitlab.collections.tiny.core.iterator;

import com.gitlab.collections.tiny.core.function.FloatConsumer;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.PrimitiveIterator;

/**
 * A specialized iterator for floats
 */
public interface FloatIterator extends PrimitiveIterator<Float, FloatConsumer> {
    /**
     * Returns the next float and advances the iterator.
     *
     * @return The next float.
     * @throws NoSuchElementException If there are no more elements.
     */
    float nextFloat();

    default void forEachRemaining(FloatConsumer action) {
        Objects.requireNonNull(action);
        while (hasNext()) {
            action.accept(nextFloat());
        }
    }
}
