package com.gitlab.collections.tiny.core.iterator;

import com.gitlab.collections.tiny.core.function.ShortConsumer;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.PrimitiveIterator;

/**
 * A specialized iterator for shorts
 */
public interface ShortIterator extends PrimitiveIterator<Short, ShortConsumer> {
    /**
     * Returns the next short and advances the iterator.
     *
     * @return The next short.
     * @throws NoSuchElementException If there are no more elements.
     */
    short nextShort();

    default void forEachRemaining(ShortConsumer action) {
        Objects.requireNonNull(action);
        while (hasNext()) {
            action.accept(nextShort());
        }
    }
}
